<?php

$app->redirect('/', 'usuarios');

$app->get('/usuarios', ['controlador' => 'Usuarios', 'funcion' => 'index']);

$app->post('/usuarios/nuevo', ['controlador' => 'Usuarios', 'funcion' => 'crear']);

$app->get('/usuarios/borrar', ['controlador' => 'Usuarios', 'funcion' => 'borrar']);

$app->error_404();
