<?php

namespace Core;

use Core\Conectar;
use Core\EntidadBase;
use Core\ModeloBase;
use Core\AyudaVistas;

class ControladorBase {

    protected $conectar;
    protected $adapter;

    public function __construct() {
        $this->conectar = new Conectar();
        $this->adapter = $this->conectar->conexion();
    }
    
    //Plugins y funcionalidades

    public function view($carpeta, $vista, $datos) {
        foreach ($datos as $id_assoc => $valor) {
            ${$id_assoc} = $valor;
        }

        $helper = new AyudaVistas();
        $view = $carpeta . '/' . $vista . '.php';
        
        $templateName = str_replace('/', '-', $carpeta) . '-' . $vista;

        require_once __DIR__.'/../resources/views/base.php';
    }

    public function redirect($controlador = CONTROLADOR_DEFECTO, $accion = ACCION_DEFECTO) {
        header("Location:/" . $controlador);
    }


    //Métodos para los controladores
}
