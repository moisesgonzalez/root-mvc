<?php

namespace Core;

class ControladorFrontal {

    private $controlador;
    private $strFileController;
    private $controladorObj;

    function __construct($controller = CONTROLADOR_DEFECTO, $accion = '', $request = []) {
        $this->controlador = ucwords($controller) . 'Controller';

        $this->controlador = '\App\Controllers\\' . $this->controlador;

        $this->controladorObj = new $this->controlador();
        
        $this->lanzarAccion($accion, $request);
    }

    private function cargarAccion($accion, $request) {
        if ($request) {
            $this->controladorObj->$accion($request);
        } else {
            $this->controladorObj->$accion();
        }
    }

    private function lanzarAccion($accion, $request) {
        if (isset($accion) && method_exists($this->controladorObj, $accion)) {
            $this->cargarAccion($accion, $request);
        } else {
            $this->cargarAccion(ACCION_DEFECTO);
        }
    }

}
