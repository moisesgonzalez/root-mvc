<?php

namespace Core;

use Core\ControladorBase;
use Core\ControladorFrontal as Controller;

class Router {

    private $url;
    private $controllerObj;
    private $rutas;
    private $domain;

    public function __construct() {     
        $uri = filter_var($_SERVER["REQUEST_URI"], FILTER_SANITIZE_STRING);
        
        $url = explode("?", $uri); 

        $this->url = $url[0];
        
        $this->domain = "http://" . filter_var($_SERVER["SERVER_NAME"], FILTER_SANITIZE_STRING);
        
        $this->rutas = array();
    }

    public function redirect($ruta, $rutaRedireccion){        
        if($this->url === $ruta){
            ControladorBase::redirect($rutaRedireccion);            
        }
    }

    public function get($ruta, $parametros) {
        $request = array_filter($_GET);
        $this->rutas[] = $ruta;

        if ($parametros) {
            $this->go($ruta, $parametros['controlador'], $parametros['funcion'], $request);
        } else {
            echo "Debe indicar un controlador y/o funcion a ejecutar";
            die();
        }
    }

    public function post($ruta, $parametros) {
        $request = array_filter($_POST);
        $this->rutas[] = $ruta;

        if ($parametros) {
            $this->go($ruta, $parametros['controlador'], $parametros['funcion'], $request);
        } else {
            echo "Debe indicar un controlador y/o funcion a ejecutar";
            die();
        }        
    }

    private function go($ruta, $controlador, $funcion, $request) {        
        if ($this->url === $ruta) {
            $this->controllerObj = new Controller($controlador, $funcion, $request);
        }
    }

    public function error_404() {
        foreach ($this->rutas as $ruta) {
            if($ruta === $this->url) {
                return;
            }
        }

        echo "<p>Error 404: Pagina no encontrada</p>";
        echo "<a href='" . $this->domain . "'>Pagina principal</a>";
        die();
    }
}
