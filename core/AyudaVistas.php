<?php
namespace Core;

class AyudaVistas{
    
    public function url($ruta){
        //$urlString="index.php?controller=".$controlador."&action=".$accion;
        $urlString = "http://" . filter_var($_SERVER["SERVER_NAME"], FILTER_SANITIZE_STRING) . "/" . filter_var($ruta, FILTER_SANITIZE_STRING);
        return $urlString;
    }
    
    //Helpers para las vistas

    public function head() {
        require_once __DIR__.'/../resources/views/layouts/head.php';        
    }

    public function menu() {
        require_once __DIR__.'/../resources/views/layouts/menu.php';        
    }

    public function footer() {
        require_once __DIR__.'/../resources/views/layouts/footer.php';
    }

    public function render($vista, $helper, $datos) {
        foreach ($datos as $id_assoc => $valor) {
            ${$id_assoc} = $valor;
        }
        
    	require_once __DIR__.'/../resources/views/templates/' . $vista;
    }
}
?>
